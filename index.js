// set up the dependencies
const express = require('express');
const mongoose =require('mongoose');
// this allows us to use all the routes defined in 'taskRoutes.js'
const taskRoute = require('./routes/taskRoute')



// server setup
const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({extended:true}))

// Database connection
mongoose.connect("mongodb+srv://dbUser:dbUser@wdc028-course-booking.0pav9.mongodb.net/batch144-to-do?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology:true
})

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"))
db.once("open", () => console.log("We're connected to the cloud database"))

// Routes(Base URI for task route)
// Allows all the task routes created in the taskRoute.js file to use "/tasks route"

app.use("/tasks", taskRoute)
// http://localhost:3001/tasks

app.listen(port, () => console.log(`Now listening to port ${port}`))