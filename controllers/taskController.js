// controllers contain the functions and business logic of our express js app 
// meaning all the operations will be placed in this file
const Task = require('../models/task');

// controller function for getting all the task
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	})
}


// Controller function for creating a task
module.exports.createTask = (requestBody) => {
	// create a task object based on the Mongoose model "Task"
	let newTask = new Task({
		name: requestBody.name
	})

	// Save
	// The "then" method will accept the 2 parameters 
		// the first parameter will store the result returned by the mongoose "save" method
		// the second parameter will store the "error" object
	return newTask.save().then((task, error) => {
		if(error){
			console.log(error)
			// if an error is encountered, the return statement will prevent any other line or other code below and within the same code block forom executing
			// since the ff return statement is nested within the then method chained to the save method, they do not prevent each other from executing code
			// the else statement will no longer be evaluated
			return false;
		} else {
			// if save is successful return the new task object
			return task;
		}
	})
}


// controller function for deleting a task
// Business Logic
// 1.look for the the task with correspdonfinf id provided in the URL.route 
// 2. Delete the task

module.exports.deleteTask = (taskId) => {
	// the "findByIdAndRemove" mongoose method will look for a task with the same id provided from the URL and remove/delte the document from MongoDB. It looks for the document using the "_id" field
	return Task.findByIdAndRemove(taskId).then((removedTask,err) => {
		if(err){
			console.log(err);
			return false;
		} else {
			return removedTask;
		}
	})
}

// Updating a task
// Business Logic
// 1.) Get the task with the ID
// 2.) replace the task's name returned from the database with the "name" property from the request body
// 3.) Save the task

module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result,error) =>{
		// if an error is encountered, return a false
		if(error){
			console.log(error);
			return false;
		}

		// results of the "findById" method will be stored in the "result" parameter
		// it's name property will be reassigned the value of the "name" received from the request
		result.name = newContent.name;

		return result.save().then((updatedTask,error) => {
			if(error){
				console.log(error);
				return false
			}else{
				return updatedTask;
			}
		})
	})
}

// Activity #2
module.exports.getTask = (taskId) => {
	return Task.findById(taskId).then(result => {
		return result;
	})
}

// 6.)
module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result,error) =>{
		// if an error is encountered, return a false
		if(error){
			console.log(error);
			return false;
		}

		// results of the "findById" method will be stored in the "result" parameter
		// it's name property will be reassigned the value of the "name" received from the request
		result.status = newContent.status;

		return result.save().then((updatedTask,error) => {
			if(error){
				console.log(error);
				return false
			}else{
				return updatedTask;
			}
		})
	})
}
